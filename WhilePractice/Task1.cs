﻿namespace WhilePractice
{
    public static class Task1
    {
        public static double SumSequenceElements(int n)
        {
            double sum = 0;
            for (int i = 1; i <= n; i++)
            {
                sum += 1 / (double)i;
            }

            return sum;
        }
    }
}
